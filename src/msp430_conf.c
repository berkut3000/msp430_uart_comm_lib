/******************************************************************************
 * @ file  msp430_conf.c
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Definition */
#define SUCCESS 0
 
/* Headers */
#include "msp430_conf.h"
#include "uart_func.h"

/* Functions */

/***************************************************************************//**
 * @brief
 *   Configuration of MSP430.
 *
 * @details
 *   Custom configuration per project is defined in this function.
 *
 * @note
 *   Clock is (must) be defined here.
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t msp430_conf()
{
    WDTCTL = WDTPW + WDTHOLD; // Stop WDT
    LED0DIR |= LED0MASK;    // Set the LEDs on P1.0, P1.6 as outputs
    // P2DIR |= BIT0;
    LED0PORT |= LED0MASK;            // Set P1.0
    LED0PORT &= ~LED0MASK;
    
    /* BCSCTL1 = CALBC1_1MHZ;   // Set DCO to 1MHz */
    /* DCOCTL = CALDCO_1MHZ;    // Set DCO to 1MHz */
    
    BCSCTL1 = CALBC1_8MHZ;   // Set DCO to 8MHz
    DCOCTL = CALDCO_8MHZ;    // Set DCO to 8MHz
    
    /* BCSCTL1 = CALBC1_16MHZ;   // Set DCO to 16MHz */
    /* DCOCTL = CALDCO_16MHZ;    // Set DCO to 16MHz */
    
    /* Configure hardware UART */
    uart_struct uart_params;
    uart_params.CLK = UCSSEL_2;
    
    
    /* 1MHz 9600 bauds */
    /* uart_params.BR0 =  104; */
    /* uart_params.BR1 = 0; */
    /* uart_params.MODUL = UCBRS_4; */
    
    /* 1MHz 115200 bauds */
    /* uart_params.BR0 =  8;
    uart_params.BR1 = 0;
    uart_params.MODUL = UCBRS_6;*/
    
    
    /* 8MHz 9600 bauds */
    /* uart_params.BR0 =  65; */
    /* uart_params.BR1 = 3; */
    /* uart_params.MODUL = UCBRS_1; */
    
    /* 8MHz 115200 bauds */
    uart_params.BR0 =  69;
    uart_params.BR1 = 0;
    uart_params.MODUL = UCBRS_3;
    
    /* 16MHz 9600 bauds */
    /* uart_params.BR0 =  130;
    uart_params.BR1 = 6;
    uart_params.MODUL = UCBRS_6;*/
    
    /* 16MHz 115200 bauds */
    /* uart_params.BR0 =  138;
    uart_params.BR1 = 0;
    uart_params.MODUL = UCBRS_7;*/
    
    uart_conf(uart_params);
	hab_in_rx();
    
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *   Enable UART reception Interrupts.
 *
 * @details
 *   Enable the UART RX Interrupt from the Interrupts Vector Register.
 *
 * @note
 *   In this example, the GPIO Interrupts are disabled.
 *
 * @param[in] none.
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t hab_in_rx()
{
    IE2 |= UCA0RXIE;
    /* P1IE &= ~SW;  */
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Disables UART recpetion Interrupts
 *
 * @details
 *   Disables the UART RX Interrupt from the Interrupts Vector Register.
 *
 * @note
 *   In this example, GPIO Pins in SW are enabled.
 *
 * @param[in] none.
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t des_in_rx()
{
    IE2 &= ~UCA0RXIE;
    /* P1IE |= SW; // P1.3 interrupt enabled */
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Enables TIMERA0 Interrupts.
 *
 * @details
 *   Enables TIMERA0 Interrupts from CCIE Register.
 *
 * @note
 *   none.
 *
 * @param[in] none
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t hab_in_tmr()
{
    CCTL0 |= CCIE;
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Disables TIMERA0 Interrupts.
 *
 * @details
 *   Disables TIMERA0 Interrupts from CCIE Register.
 *
 * @note
 *   none.
 *
 * @param[in] none
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t des_in_tmr()
{
    CCTL0 &= ~CCIE;
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO1 Interrupts.
 *
 * @details
 *   Enable the GPIO1 Interrupts from the Interrupts Vector Register, related
 *		to the specified pin.
 *
 * @note
 *
 *
 * @param[in] gpio.
 *	Specified GPIO Pin Number
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t hab_in_P1(uint8_t gpio)
{
    P1IE |= gpio;
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO1 Interrupts.
 *
 * @details
 *   Enable the GPIO1 Interrupts from the Interrupts Vector Register, related
 *		to the specified pin.
 *
 * @note
 *
 *
 * @param[in] gpio.
 *	Specified GPIO Pin Number
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t des_in_P1(uint8_t gpio)
{
    P1IE &= ~gpio;
	return 0;
}

/***************************************************************************//**
 * @brief
 *	Sets the MCU to sleep.
 *
 * @details
 *	Sets the MCU to Low-Power Mode with Interrupts enabled.
 *
 * @note
 *	LPMO, LPM2, LPM3 and LPM4 are acceptable Power Modes,  refer to page 39
 *	of the slau144 document (MSP430 Family User's Guide) for detailed info
 *	about any of the aforementioned modes.
 *
 * @param[in] mode
 *   LPMX Low-Power Mode.
 *
 * @return
 *   Nothing.
 ******************************************************************************/
void dormir(uint8_t mode)
{
    __bis_SR_register(mode + GIE); // Enter LPM0, interrupts enabled
}

/***************************************************************************//**
 * @brief
 *	Wakes up the MCU.
 *
 * @details
 *	Wakes the MCU from the Low-Power Mode.
 *
 * @note
 *	LPMO, LPM2, LPM3 and LPM4 are acceptable Power Modes,  refer to page 39
 *	of the slau144 document (MSP430 Family User's Guide) for detailed info
 *	about any of the aforementioned modes.
 *
 * @param[in] mode
 *   LPMX Low-Power Mode.
 *
 * @return
 *   Nothing.
 ******************************************************************************/
/* void wake(uint8_t mode)
 {
	 __bic_SR_register_on_exit(mode);
 } */
