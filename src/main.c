/* Example code demonstrating the use of pointers.
 * It uses the hardware UART on the MSP430G2553 to receive
 * and transmit data back to a host computer over the USB connection on the MSP430
 * launchpad.
 * Note: After programming using CCS it is necessary to stop debugging and reset the uC before
 * connecting the terminal program to transmit and receive uint8_tacters.
 * This demo will transmit the uint8_tacters AB in response to a S been sent each time S is sent the
 * order of the uint8_tacters is swaped. If agen any other uint8_tacter is sent and unknown command response is sent.
 */

#include "msp430g2553.h"
#include "uart_func.h"
#include "msp430_conf.h"
#include <stdint.h>

void SwapChars(uint8_t *a, uint8_t *b);

//volatile uint8_t bufferghini[RNG_BUFFER_SIZE] = "";

void main(void)
{
    /* uint8_t bufferghini[20]; */
    /* uint8_t * p_buff; */
    uint8_t x1 = '0';
    uint8_t x2 = '2';
    uint8_t Data[RNG_BUFFER_SIZE] = "";
    memset(&bufferghini,'0',RNG_BUFFER_SIZE);
    msp430_conf();
    
    uart_send_string("Unknown Command: ");
    
    while(1)
    {
        //__bis_SR_register(LPM0_bits + GIE); // Enter LPM0, interrupts enabled
        uart_read((uint8_t *)&Data, 20);
//        uart_send_array( (uint8_t *)&Data, 50 );
        uart_send_array( (uint8_t *)&bufferghini, 50 );
        uart_send_array("\r\n", 2);
        
        // /* (uint8_t *)bufferghini = Data; */
        // switch(Data)
        // {
            // case 'S':
            // {
                // SwapChars(&x1,&x2);     // Pass the pointers to variables x1 and x2 to the function
                // uart_send_byte(&x1);
                // uart_send_byte(&x2);
                // uart_send_array("\r\n", 2);
                // LED0PORT &= ~LED0MASK;            // Set P1.0
                // }
                // break;
                // default:
            // {
                // uart_send_array("Unknown Command: ", 17);
                
            // }
            // break;
        // }
        // LED0PORT ^= LED0MASK;
        
    }
}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    if(RNG_BUFFER_SIZE <= buffer_index)
    {
        buffer_index = 0;
    }
    buff_end = buffer_index;
    bufferghini[buffer_index++] = UCA0RXBUF;
    
    LED0PORT ^= LED0MASK;
    __bic_SR_register_on_exit(LPM0_bits);
}

void SwapChars(uint8_t *a, uint8_t *b)
{
    uint8_t c = 0;      // initialise temporary variable c
    P1OUT |= BIT6;            // Set P1.0
    c = *a;      // copy the value in memory location a in variable c
    *a = *b;     // copy the value stored in memory location b into memory location a
    *b = c;      // copy the value temporarily stored in c into memory location b
}