/******************************************************************************
 * @ file  uart_func.c
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

 /* Definitions */
#define SUCCESS 0;
 
/* Headers */
#include <string.h>
#include "uart_func.h"
#include "msp430_conf.h"

volatile uint8_t bufferghini[RNG_BUFFER_SIZE] = "";
volatile uint8_t buffer_index = 0;
volatile uint8_t buff_st = 0;
volatile uint8_t buff_end = 0;

/***************************************************************************//**
 * @brief
 *   Configures UART0.
 *
 * @details
 *   Configures UART0 according to the parameters set in uart_struct.
 *
 * @note
 *   BR0 and BR1 must be correclty set, use the online calculator if in doubt.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t uart_conf(uart_struct uart_strct)
{
    P1SEL = BIT1 + BIT2;            // P1.1 = RXD, P1.2=TXD
    P1SEL2 = BIT1 + BIT2;           // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 |= uart_strct.CLK;     // Use xCLK
    UCA0BR0 = uart_strct.BR0;       // Set BaudRate Upper register
    UCA0BR1 = uart_strct.BR1;       // Set BaudRate Upper register
    UCA0MCTL = uart_strct.MODUL;    // Modulation UCBRSx = 1
    UCA0CTL1 &= ~UCSWRST;           // Initialize USCI state machine
    
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Send an array of bytes.
 *
 * @details
 *   Send number of bytes Specified in array_length in the array at using 
 *      the hardware UART 0.
 *
 * @note
 *   Be careful with number of bytes transmitted.
 *
 * @param[in] p_tx_array
 *   Pointer referencing the array to be trasnmitted.
 *
 * @param[in] array_length
 *  Number of bytes to be transmitted.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t uart_send_array(uint8_t *p_tx_array, uint8_t array_length)
{
    while(array_length--)
    {
        while(!(IFG2 & UCA0TXIFG));     /* Wait for TX buffer to be ready */
        UCA0TXBUF = *p_tx_array++;         /* Write the character */
    }
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Send a String.
 *
 * @details
 *   Sends an array which exclusively contains string characters through the
 *      the hardware UART 0.
 *
 * @note
 *   Use it for not specyfying the number of bytes to be transmitted. No NULL's 
 *      inside the string are accepted.
 *
 * @param[in] array_length
 *  Number of bytes to be transmitted.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t uart_send_string(uint8_t *p_tx_string)
{
   uint8_t tama = 0;
   tama = strlen((char *)p_tx_string);
   
   for(int i = 0; i < tama; i++)
   {
       while(!(IFG2 & UCA0TXIFG));     /* Wait for TX buffer to be ready */
       UCA0TXBUF = *p_tx_string++;         /* Write the character */
   }
   
   return SUCCESS;
   
}

/***************************************************************************//**
 * @brief
 *   Send a byte.
 *
 * @details
 *   Sends a singlebyte from the specified pointer through the UART.
 *
 * @note
 *   None.
 *
 * @param[in] *p_tx_byte
 *   Values passed as parameters to the function.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t uart_send_byte(uint8_t *p_tx_byte)
{
    while(!(IFG2 & UCA0TXIFG));     /* Wait for TX buffer to be ready */
    UCA0TXBUF = *p_tx_byte;        /* Write the uint8_tacter */
    
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *    Function to read from the UART.
 *
 * @details
 *   Reads the number of specified bytes through the UART
 *
 * @note
 *   Use of the ring buffer is implemented, ensure to pass the read value to the
 *      global buffer and the global buffer index. Also, check the RNG_BUFF_SIZE
 *
 * @param[in] data_buff
 *   Pointer to store the contents of the read buffer.
 *
 * @param[in] data_size
 *   Number of bytes to be read.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t uart_read(uint8_t * data_buff, uint8_t data_size)
{
    uint8_t iteracion = 0;
    if(RNG_BUFFER_SIZE < data_size)
    {
        return 1;
        //There will be lost data, therefore the buffer won't be properly read
    }
    buff_st = buff_end;
    while(iteracion <= data_size)
    {
        dormir(LPM0_bits);
        iteracion++;
    }
    return SUCCESS;
}





