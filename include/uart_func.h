/******************************************************************************
 * @ file  uart_func.h
 *
 *  Created on: 06/09/2017
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __UART_FUNC_H__
#define __UART_FUNC_H__

#include "msp430g2553.h"
#include <stdint.h>

typedef struct {
	uint8_t CLK;
	uint8_t BR0;
	uint8_t BR1;
	uint8_t MODUL;	
}uart_struct;

#define RNG_BUFFER_SIZE 50

/* External global variables require for the Ring buffer */
extern volatile uint8_t bufferghini[RNG_BUFFER_SIZE];
extern volatile uint8_t buffer_index;
extern volatile uint8_t buff_st;
extern volatile uint8_t buff_end;


uint8_t uart_conf(uart_struct uartStruct);  /* UART Setup function */
uint8_t uart_send_array(uint8_t *p_tx_array, uint8_t array_length); /* Function to send arrays */
uint8_t uart_send_string(uint8_t *p_tx_string); /* Function to send strings */
uint8_t uart_send_byte(uint8_t *p_tx_byte);/* Function to send a single byte */
uint8_t uart_read(uint8_t * data_buff, uint8_t data_size); /* Function to read from the UART */

#endif /* __UART_FUNC_H__ */
